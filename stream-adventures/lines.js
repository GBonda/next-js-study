const split2 = require("split2");
const through = require("through2");

// const stream = through(write, end);

// function write(buffer, encoding, next) {
//   this.push(buffer.toString());
//   next();
// }

// function end(done) {
//   done();
// }

let isOdd = true;
process.stdin.pipe(split2()).pipe(
  through(function (line, _, next) {
    if (isOdd) {
        this.push(line.toString().toLowerCase()+ '\n')
    //   console.log(line.toString().toLowerCase());
    } else {
        // console.log(line.toString().toUpperCase());
        this.push(line.toString().toUpperCase()+ '\n')
    }
    isOdd = !isOdd;
    next();
  })
)
//   .pipe(stream)
.pipe(process.stdout);

// process.stdin.pipe(stream).pipe(process.stdout)


//stream-adventure run lines.js
//stream-adventure verify lines.js
