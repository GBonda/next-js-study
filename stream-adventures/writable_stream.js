const { Writable, Readable } = require("stream");

const readable = new Readable({});
readable._read = () => {
};

const writable = new Writable({  
    write(chunk, encoding, callback) {
        console.log('writing: ' + chunk);
        callback();
    }  
  }) 

// readable.on('data', (chunk) => {
//     writable.write(chunk)
//   })

  process.stdin.pipe(writable)

  //stream-adventure run writable_stream.js