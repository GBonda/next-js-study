const trumpet = require('trumpet');
const through = require('through2');

const tr = trumpet();
process.stdin.pipe(tr);
const trStream = tr.select('.loud').createStream();
const troughStream = through(write, end);
function write(buffer, encoding, next) {
    this.push(buffer.toString().toUpperCase())
    next()
}
function end(done) {
    done()
}

trStream.pipe(troughStream).pipe(trStream);

tr.pipe(process.stdout);
