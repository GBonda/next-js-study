const combine = require("stream-combiner");
const split2 = require("split2");
const through = require("through2");
const zlib = require("zlib");

module.exports = function () {
  let resObj = {};
  const stream = through({ objectMode: true }, write, end);

  function write(json, encoding, next) {
    // console.log(json.toString());
    if (json.length === 0) return next();
    const obj = JSON.parse(json);
    // console.log("main", obj);
    if (obj["type"] === "genre") {
    //   console.log("first if", obj);
      resObj.name = obj.name;
      if (resObj.books) {
        // console.log(obj);
        this.push(JSON.stringify(resObj) + "\n");
        // this.push(JSON.stringify(resObj));
        // this.push(resObj);
        // this.push(obj);
        // console.log(resObj);
        resObj = {};
      }
    } else if (obj.type === "book") {
    //   console.log("second if", obj);
      //   console.log(obj.name);
      if (!resObj.books) {
        // console.log(resObj);
        resObj.books = [];
      }
      resObj.books.push(obj.name);
      //   console.log(resObj);
      //   this.push(resObj);
    }
    // console.log(resObj);
    next();
  }

  function end(done) {
    if (resObj.books) {
        // console.log(obj);
        this.push(JSON.stringify(resObj) + "\n");
      }
    // zlib.createGzip();
    done();
  }
  //    console.log(this);
  // return combine(zlib.createGzip(), stream, split2(), process.stdin);
//   return combine(stream, split2(), process.stdin);
  return combine(split2(), stream, zlib.createGzip());
  // read newline-separated json,
  // group books into genres,
  // then gzip the output
};
