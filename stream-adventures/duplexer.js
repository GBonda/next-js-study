const { spawn } = require('child_process') 
const duplexer = require("duplexer2");
       
     module.exports = function (cmd, args) {  
        const process = spawn(cmd, args);
        return duplexer(process.stdin, process.stdout);
       // spawn the process and return a single stream  
       // joining together the stdin and stdout here  
     }  