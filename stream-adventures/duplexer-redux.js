const duplexer = require("duplexer2");
const through = require("through2");



module.exports = function (counter) {  
    // return a duplex stream to count countries on the writable side  
    // and pass through `counter` on the readable side 
    const resObj = {};
    const stream = through({ objectMode: true}, write, end);

function write(obj, encoding, next) {
    // console.log(obj);
    if (resObj && obj.country in resObj) {
        resObj[obj.country] = resObj[obj.country] + 1;
    } else {
        resObj[obj.country] = 1;
    }
    next()
}

function end(done) {
    counter.setCounts(resObj);
    done()
}
 
    return duplexer({ objectMode: true}, stream, counter);
    // return duplexer(process.stdout, counter);
    
  }  