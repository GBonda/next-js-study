const concat = require("concat-stream");
const { Readable } = require("stream");

const myStream = new Readable({});
myStream._read = () => {};

process.stdin.pipe(
  concat(function (body) {
    myStream.push(body.toString().split("").reverse().join(""));
    // console.log(body.toString());
  })
);
myStream.pipe(process.stdout);

// const concat = require("concat-stream");

// process.stdin.pipe(
//   concat(function (src) {
//     const s = src.toString().split("").reverse().join("");
//     process.stdout.write(s);
//   })
// );
