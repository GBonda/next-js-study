import { prepare, createServer } from "./src/controller.mjs";
import dotenv from "dotenv";

dotenv.config();

const PORT = process.env.PORT;

if (!PORT) {
  console.error("PORT environment variable is missing");
  process.exit(1);
}

const dir = await prepare();
await createServer(dir, process.env.PORT);
