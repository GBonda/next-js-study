import { open, unlink, readdir, stat } from "node:fs/promises";
import { randomUUID } from "node:crypto";
import { extension } from "mime-types";
import path, { join } from "node:path";

export const createFile = async (req, ASSETS_DIR_PATH, wsServer) => {
  const fileName = `${randomUUID()}.${extension(req.headers["content-type"])}`;
  const file = await open(join(ASSETS_DIR_PATH, fileName), "a");
  const writable = file.createWriteStream();
  req.pipe(writable);
  // wsServer.broadcast(`File added ${fileName}`);
  return fileName;
};

export const listFiles = async (ASSETS_DIR_PATH) => {
  return await readdir(ASSETS_DIR_PATH);
};

export const getFileReadStream = async (ASSETS_DIR_PATH, imageName) => {
  const file = await open(
    join(ASSETS_DIR_PATH, imageName),
    "r"
  )
  return file.createReadStream();
};

export const getFileSize = async (ASSETS_DIR_PATH, imageName) => {
  return (await stat(path.join(ASSETS_DIR_PATH, imageName))).size;
}

export const deleteFile = async (ASSETS_DIR_PATH, imageName, wsServer) => {
  await unlink(path.normalize(join(ASSETS_DIR_PATH, imageName)));
  // wsServer.broadcast(`File deleted ${imageName}`);
};
