import { Router } from 'express';
import { createFile, deleteFile, getFileReadStream, getFileSize } from '../model.mjs';
import { contentType } from 'mime-types';
import { addImageNotify, removeImageNotify } from '../notifications.mjs';
import { PassThrough } from 'node:stream';
import Redis from 'ioredis';

const redisClient = new Redis();

const imageRouter = (ASSETS_DIR_PATH, logger) => {
  const router = new Router();

  router.post('/', async (req, res, next) => {
    const data = await createFile(req, ASSETS_DIR_PATH);
    addImageNotify(data);

    logger.debug(`Data from createFile: ${JSON.stringify(req.body)}`);

    res.setHeader('Content-Type', 'text/plain');
    res.setHeader('Content-Length', data.length);
    res.statusCode = 201;
    res.end(data);
  });

  router
    .route('/:imageName')
    .get(async (req, res, next) => {
      const imageName = req.params.imageName;
      const file = await redisClient.getBuffer(`file:${imageName}`);

      if (file) {
        console.log('Cache HIT!');

        res.setHeader('Content-Type', contentType(imageName));
        res.setHeader('Content-Disposition', `inline`);
        return res.send(file);
      }

      try {
        const fileSize = await getFileSize(ASSETS_DIR_PATH, imageName);
        const fileReadStream = await getFileReadStream(ASSETS_DIR_PATH, imageName);
        res.setHeader('Content-Type', contentType(imageName));
        res.setHeader('Content-Disposition', `inline`);
        // fileReadStream.pipe(res);

        const passThrough = new PassThrough();
        passThrough.on('data', chunk => {
          redisClient.append(`file:${imageName}`, chunk);
        });

        passThrough.on('end', () => {
          redisClient.expire(`file:${imageName}`, 60);
        });

        fileReadStream.pipe(passThrough).pipe(res);

      } catch (e) {
        e.filename = imageName;
        next(e);
      }
    })
    .delete(async (req, res, next) => {
      const imageName = req.params.imageName;
      try {
        await deleteFile(ASSETS_DIR_PATH, imageName);
        removeImageNotify(imageName);
        res.statusCode = 204;
        res.end();
      } catch (e) {
        next(e);
      }
    });

  return router;
}

export default imageRouter;
