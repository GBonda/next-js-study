import { access, mkdir } from "node:fs/promises";
import { server as WebSocketServer } from "websocket";
import morgan from 'morgan';
import path, { join } from "node:path";
import {
  listFiles,
} from "./model.mjs";
import express from "express";
import imageRouter from "./routes/image.mjs";
import winston from 'winston';
import hbs from 'hbs';
import { onImageAdd, onImageRemove } from './notifications.mjs';
import session from 'express-session';
import ConnectRedis from 'connect-redis';
import Redis from 'ioredis';

const RedisStore = ConnectRedis(session);
const sessionRedisClient = new Redis();

const redisClient = new Redis();

const logger = winston.createLogger({
  level: process.env.LOG_LEVEL ?  process.env.LOG_LEVEL : process.env.NODE_ENV === 'production' ? 'error' : 'debug',
  transports: [
    new winston.transports.Console(),
  ]
});


const httpLogger = morgan('tiny');

export const prepare = async () => {
  const ASSETS_DIR_NAME = "images";
  const ASSETS_DIR_PATH = path.normalize(join(".", ASSETS_DIR_NAME));

  const hasAssetsDir = await access(ASSETS_DIR_PATH).then(
    () => true,
    () => false
  );

  if (!hasAssetsDir) {
    await mkdir(ASSETS_DIR_PATH);
  }

  return ASSETS_DIR_PATH;
};

export const createServer = async (ASSETS_DIR_PATH, port) => {
  const app = express();

  app.set('view engine', 'hbs');

  app.use(httpLogger);

  // const filenameExtractor = (req, res, next) => {
  //   const filename = req.params.filename || 'index.html';
  //   req.body = { filename };
  //   next();
  // }

  app.use(express.json());

  app.use(session({
    store: new RedisStore({ client: sessionRedisClient }),
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
  }));

  // app.use(async (req, res, next) => {
  //   if (!req.session.userId) {
  //     req.session.userId = await redisClient.incr('userId');
  //   }
  //   next();
  // });

  app.post('/login', async (req, res, next) => {
    if (req.body.username === 'username' && req.body.password === 'password') {
        req.session.userId = await redisClient.incr('userId');
        res.sendStatus(204);
    } else {
      const error = new Error('wrong credentials');
      error.statusCode = 403;
      next(error);
    }
  });

  

  app.get('/images', async (req, res, next) => {
    const data = await listFiles(ASSETS_DIR_PATH);
    console.log(req.session);
    res.render('list', { images: data });
  });

  app.use('/image', imageRouter(ASSETS_DIR_PATH, logger));

  app.use(express.static('swagger'));

  app.use((error, req, res, next) => {
    // res.statusCode = 404;
    console.error(error);
    if (error.statusCode) {
      res.statusCode = error.statusCode;
    } else {
      res.statusCode = 404;
    }

    res.render('404', { filename: error.filename ? error.filename : 'File' });
  });

  const server = app.listen(port, () => {
    console.log(`Server on port ${server.address().port}`);
  });

  const wsServer = new WebSocketServer({ httpServer: server, autoAcceptConnections: true });

  wsServer.on('connect', () => {
    logger.debug(`Websocket client connected to process ${process.pid}`);
  });


  onImageAdd((channel, payload) => wsServer.broadcast(`Image was added: ${payload}`));
  onImageRemove((channel, payload) => wsServer.broadcast(`Image was removed: ${payload}`));


  return server;
};
