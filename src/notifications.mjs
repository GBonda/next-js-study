import { EventEmitter } from 'node:events';
import Redis from 'ioredis';

const pubClient = new Redis();
const subClient = new Redis();
const sub2Client = new Redis();

const notificationsEventEmitter = new EventEmitter();

const EventTypes = {
  IMAGE_ADD: 'ImageAdd',
  IMAGE_REMOVE: 'ImageRemove',
}

export const addImageNotify = (payload) => pubClient.publish(EventTypes.IMAGE_ADD, payload);
export const removeImageNotify = (payload) => pubClient.publish(EventTypes.IMAGE_REMOVE, payload);

export const onImageAdd = (cb) => {
  subClient.subscribe(EventTypes.IMAGE_ADD);
  subClient.on('message', cb);
};
export const onImageRemove = (cb) => {
  sub2Client.subscribe(EventTypes.IMAGE_REMOVE);
  sub2Client.on('message', cb);
};
